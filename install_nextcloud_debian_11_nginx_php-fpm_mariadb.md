# Install Nextcloud Debian 11 (Bullseye) Nginx php-fpm Mariadb

## Index

1. Index
1. Vorwort
   1. ^
1. Datenbank (MariaDB)
   1. Installaton
   1. Passwort
   1. Datenbank erstellen
   1. Datenbank User erstellen
   1. Datenbank User rechte vergeben
   1. Datenbank Verbindung testen
1. PHP-FPM
   1. Installation
   1. Testen
1. Weitere Packete
1. Nginx + Certbot
   1. Installation
   1. Vhost
      1. Vhost htdocs
      1. Vhost Datei
1. Web Installer
1. Quellen

## Vorwort

### ^

Das `^` steht hier meist für die `STRG`-Taste. Wenn also `^d` angegeben ist, ist damit gemeint das man die `STRG`-Taste plus die `d`-Taste drücken soll.

## Datenbank

### Installaton

Als erstes müssen wir die Packete Installieren, die für die Datenbank benötigt werden. Da wir hier Mariadb einsetzten, reicht und das Packet `mariadb-server`

```bash
apt install mariadb-server
```

### Passwort

Damit wir das Passwort nie Sichtbar in der Console oder der Bash History haben, setzten wir dies `read`. `read` hat dafür auch extra die Option `-s`.
Nun lesen wir das Passwort für den Nextcloud DB User in die Variable `nc_db_user_pass` ein.

```bash
read -s nc_db_user_pass
```

### Datenbank erstellen

Als nächstes müssen wir die Datenbank `nextcloud` erstellen, wenn diese nicht existiert.
Dabei setzten wir gleich `CHARACTER utf8mb4` und `COLLATE utf8mb4_general_ci`.

```bash
mysql --execute="CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
```

### Datenbank User erstellen

Nun legen wir den Datenbank Benutzer `nextcloud`  an und vergeben mit der, zuvor deklarierten Variable `nc_db_user_pass` das Passwort.

```bash
mysql --execute="CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY '${nc_db_user_pass?}';"
```

### Datenbank User rechte vergeben

Abschließend müssen wir dem Benutzer `nextcloud` alle Rechte auf die `nextcloud` Datenbank gewähren.

```bash
mysql --execute="GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'localhost';"
```

### Testen

Abschließend sollten wir Testen ob der Zugriff über den User mit dem Passwort klappt.
Bei dem folgenden Befehl muss das Password manuell angegeben werden.

```bash
mysql --host=localhost --password nextcloud
```

Solte folgender Output ersichtlich sein, hat alles geklappt und man kann die MySQL Console wieder mit ^d verlassen.

```bash
Enter password:
…
MariaDB [nextcloud]>
```

## PHP-FPM

### Installation

Da wir `nginx` nutzten wollen, wird dieser `php-fpm` benötigen; Da der Websever über `php-fpm` die php Dateien interpretieren lässt.

Unter Debian 11 wir bei der Installation von `php-fpm` die Version 7.4 von PHP installiert.
Dadurch werden eine Angaben später, nicht immer `php-fpm` lauten sindern teilweise die Versionsnummer beherbergen. Bspw. `php7.4-fpm`.

Dafür sind folgende Packete notwendig:

```bash
apt install php-fpm php-gd php-mysql php-curl php-xml php-zip php-intl php-mbstring php-bz2 php-json php-apcu php-imagick php-gmp php-bcmath
```

### Testen

Für das Testen reicht es uns hier, wenn die `systemd`-Unit `php7.4-fpm` läuft.

```bash
systemctl status php7.4-fpm.service
```

## Weitere Packete

Für die Nextcloud sind weitere Packete notwendig die wir wie folgt installieren:

```bash
apt install imagemagick libmagickcore-6.q16-6-extra
```

## Nginx + Certbot

### Installation

Da Webserver und der Einsatz von SSL heute Hand in Hand gehen, Behandeln wird dies hier auch in einem Schritt. `nginx` wird unser Webserver sein und `certbot` benutzen wir, um uns ein Certifikat zu holen.
Die benötigten Pakete sind hierbei folgende:

```bash
apt install nginx certbot python3-certbot-nginx
```

### Vhost

Nun brauchen wir einen Vhost, sprich eine Configuration für den Webserver, auf welche Domain er wie reagieren soll.

Bei Nginx gehören diese Kanfiguration unter Debian in das Verzeichnis `/etc/nginx/sites-available`.
Wenn die dort hinterlegte Konfiguration von Nginx auch aktive berücksichtigt werden soll, setzt man dafür unter `/etc/nginx/sites-enabled` einen Symlink auf die Datei.

Wir gehen im folgendem davon aus, das die Domain für die Nextcloud `nextcloud.example.com` lauten soll.

#### Vhost htdocs

Bevor wir einen VHost Konfigurieren können, brauchen wir aber ein Verzeichnis, auf das dieser Verweisen soll. Dies wird in Zukunft `htdocs` genannt und wie folgt angelegt:

```bash
mkdir --parents "/var/www/nextcloud.example.com/htdocs"
```

Damit der Webserver in diesem Arbeiten kann, müssen wir den Ordner noch dem User vom Webserver unter Debian gegeben.

```bash
chown --recursive www-data: "/var/www/nextcloud.example.com"
```

Damit wir auch hier etwas sehen können, hinterlegen wir uns auch hier die Default-Index seite:

```bash
mv --verbose "/var/www/html/index.nginx-debian.html" "/var/www/nextcloud.example.com/htdocs/"
```

#### Vhost Datei

Die Vhost Datei legen wir wie folgt an:

```bash
vim /etc/nginx/sites-available/nextcloud.example.com.conf
```

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name nextcloud.example.com;

	root /var/www/nextcloud.example.com/htdocs;
	index index.html index.nginx-debian.html;
}
```

Damit diese nun aktive von Nginx berücksichtigt wird, müssen wir diese im `…/sites-enabled` Verzeichnis hinterlegen und Nginx triggern.

```bash
ln --symbolic /etc/nginx/sites-available/nextcloud.example.com.conf /etc/nginx/sites-enabled/
nginx -t # Tested die Konfiguration
systemctl reload nginx.service
```

#### Vhost testen

Damit wir sicher sein können, das der neue Vhost Funktioniert, müssen wir diesen Testen. Dafür bietet sich `curl` an und eine kleine Veränderung in der Index-Datei.

Als erstes ändern wir in der Index-Datei mit `sed` den Titel `Welcome to nginx!` in `Welcome to Nextcloud!`:

```bash
sed -i 's/Welcome to nginx!/Welcome to Nextcloud!/g' /var/www/nextcloud.example.com/htdocs/index.nginx-debian.html
```

Danach können wir mit Curl testen, ob wir diesen Titel auch sehen, wenn wir die Seite aufrufen:

```
curl 'http://nextcloud.example.com/'
```

### Certbot

Ab hier müssen wir davon ausgehen das die Verwendete Domain `nextcloud.example.com` auf den Webserver verweist.
Dann können wir nun mit `certbot` uns ein Zertifikat holen.

```bash
certbot --nginx --email webmaster@example.com --agree-tos --no-eff-email --non-interactive --redirect --domain "nextcloud.example.com"
```

#### Certbot testen

```bash
curl 'https://nextcloud.example.com/'
```

## Download Nextcloud

Nun können wir uns die Nextcloud herrunter laden und entpacken:

```bash
curl 'https://download.nextcloud.com/server/releases/latest.tar.bz2' | tar xvj --strip-components=1 -C /var/www/nextcloud.example.com/htdocs/
```

Die Dateien müssen nun dem richtigen Benutzer übergeben werden:

```bash
chown --recursive www-data: "/var/www/nextcloud.example.com/htdocs"
```

Nun muss unsere Vhost config erweitert werden.

Ganz oben, vor den `server` Bereichen kommt folgendes Hinzu:

```nginx
upstream php-handler {
    server unix:/var/run/php/php7.4-fpm.sock;
}

# Set the `immutable` cache control options only for assets with a cache busting `v` argument
map $arg_v $asset_immutable {
    "" "";
    default "immutable";
}
```

In der Config gibt es nun einen Bereich für HTTP unter Port 80 und einen für HTTPS mit Port 443.
Der Vhost welcher auf HTTPS (Port 443) regiert, wird nun um folgenden Abschnitt erweitert:

```nginx
…

# set max upload size and increase upload timeout:
client_max_body_size 512M;
client_body_timeout 300s;
fastcgi_buffers 64 4K;

# Enable gzip but do not remove ETag headers
gzip on;
gzip_vary on;
gzip_comp_level 4;
gzip_min_length 256;
gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/wasm application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

# Pagespeed is not supported by Nextcloud, so if your server is built
# with the `ngx_pagespeed` module, uncomment this line to disable it.
#pagespeed off;

# HTTP response headers borrowed from Nextcloud `.htaccess`
add_header Referrer-Policy                      "no-referrer"   always;
add_header X-Content-Type-Options               "nosniff"       always;
add_header X-Download-Options                   "noopen"        always;
add_header X-Frame-Options                      "SAMEORIGIN"    always;
add_header X-Permitted-Cross-Domain-Policies    "none"          always;
add_header X-Robots-Tag                         "none"          always;
add_header X-XSS-Protection                     "1; mode=block" always;

# Remove X-Powered-By, which is an information leak
fastcgi_hide_header X-Powered-By;

# Specify how to handle directories -- specifying `/index.php$request_uri`
# here as the fallback means that Nginx always exhibits the desired behaviour
# when a client requests a path that corresponds to a directory that exists
# on the server. In particular, if that directory contains an index.php file,
# that file is correctly served; if it doesn't, then the request is passed to
# the front-end controller. This consistent behaviour means that we don't need
# to specify custom rules for certain paths (e.g. images and other assets,
# `/updater`, `/ocm-provider`, `/ocs-provider`), and thus
# `try_files $uri $uri/ /index.php$request_uri`
# always provides the desired behaviour.
index index.php index.html /index.php$request_uri;

# Rule borrowed from `.htaccess` to handle Microsoft DAV clients
location = / {
    if ( $http_user_agent ~ ^DavClnt ) {
        return 302 /remote.php/webdav/$is_args$args;
    }
}

location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
}

# Make a regex exception for `/.well-known` so that clients can still
# access it despite the existence of the regex rule
# `location ~ /(\.|autotest|...)` which would otherwise handle requests
# for `/.well-known`.
location ^~ /.well-known {
    # The rules in this block are an adaptation of the rules
    # in `.htaccess` that concern `/.well-known`.

    location = /.well-known/carddav { return 301 /remote.php/dav/; }
    location = /.well-known/caldav  { return 301 /remote.php/dav/; }

    location /.well-known/acme-challenge    { try_files $uri $uri/ =404; }
    location /.well-known/pki-validation    { try_files $uri $uri/ =404; }

    # Let Nextcloud's API for `/.well-known` URIs handle all other
    # requests by passing them to the front-end controller.
    return 301 /index.php$request_uri;
}

# Rules borrowed from `.htaccess` to hide certain paths from clients
location ~ ^/(?:build|tests|config|lib|3rdparty|templates|data)(?:$|/)  { return 404; }
location ~ ^/(?:\.|autotest|occ|issue|indie|db_|console)                { return 404; }

# Ensure this block, which passes PHP files to the PHP process, is above the blocks
# which handle static assets (as seen below). If this block is not declared first,
# then Nginx will encounter an infinite rewriting loop when it prepends `/index.php`
# to the URI, resulting in a HTTP 500 error response.
location ~ \.php(?:$|/) {
    # Required for legacy support
    rewrite ^/(?!index|remote|public|cron|core\/ajax\/update|status|ocs\/v[12]|updater\/.+|oc[ms]-provider\/.+|.+\/richdocumentscode\/proxy) /index.php$request_uri;

    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
    set $path_info $fastcgi_path_info;

    try_files $fastcgi_script_name =404;

    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param PATH_INFO $path_info;
    fastcgi_param HTTPS on;

    fastcgi_param modHeadersAvailable true;         # Avoid sending the security headers twice
    fastcgi_param front_controller_active true;     # Enable pretty urls
    fastcgi_pass php-handler;

    fastcgi_intercept_errors on;
    fastcgi_request_buffering off;

    fastcgi_max_temp_file_size 0;
}

location ~ \.(?:css|js|svg|gif|png|jpg|ico|wasm|tflite|map)$ {
    try_files $uri /index.php$request_uri;
    add_header Cache-Control "public, max-age=15778463, $asset_immutable";
    access_log off;     # Optional: Don't log access to assets

    location ~ \.wasm$ {
        default_type application/wasm;
    }
}

location ~ \.woff2?$ {
    try_files $uri /index.php$request_uri;
    expires 7d;         # Cache-Control policy borrowed from `.htaccess`
    access_log off;     # Optional: Don't log access to assets
}

# Rule borrowed from `.htaccess`
location /remote {
    return 301 /remote.php$request_uri;
}

location / {
    try_files $uri $uri/ /index.php$request_uri;
}

…
```

Dies kann man sich hier aber auch noch mal Ansehen: https://docs.nextcloud.com/server/latest/admin_manual/installation/nginx.html#nextcloud-in-the-webroot-of-nginx

```bash
nginx -t # Tested die Konfiguration
systemctl reload nginx.service
```

## Web Installer

Nun kann der Webinstaller aufgerufen werden:

https://nextcloud.example.com/

Nach Abschluss des Installers, steht die Nextcloud bereit.

## Quellen

1. https://decatec.de/home-server/nextcloud-auf-ubuntu-server-20-04-lts-mit-nginx-mariadb-php-lets-encrypt-redis-und-fail2ban/#Konfiguration_MariaDB
1. https://docs.nextcloud.com/server/latest/admin_manual/installation/nginx.html#nextcloud-in-the-webroot-of-nginx
